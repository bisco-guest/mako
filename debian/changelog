mako-notifier (1.9.0-1) unstable; urgency=medium

  * New upstream release

 -- Birger Schacht <birger@debian.org>  Sun, 12 May 2024 12:29:21 +0200

mako-notifier (1.8.0-2) unstable; urgency=medium

  * Add d/conffiles to remove apparmor file (Closes: #1040352)
  * Fix d/gitlab-ci.yml to work with bare layout.

 -- Birger Schacht <birger@debian.org>  Sun, 03 Sep 2023 18:24:04 +0200

mako-notifier (1.8.0-1) unstable; urgency=medium

  * New upstream release
  * d/control:
   + Replace libgdk-pixbuf2.0-dev with libgdk-pixbuf-2.0-dev
     (Closes: #1037394)
   + Bump standards version to 4.6.2 (no changes required)
  * d/changelog
   + Update copyright years
  * Fix lintian override

 -- Birger Schacht <birger@debian.org>  Sun, 25 Jun 2023 09:32:14 +0200

mako-notifier (1.7.1-1) unstable; urgency=medium

  * New upstream release
  * Bump Standards-Version to 4.6.1.0 (no changes required)
  * Extend copyright years to 2022 in d/copyright
  * Remove apparmor file from d/mako-notifier.install which upstream has
    dropped
  * Update patch to install zsh completions in correct directory
  * Update watchfile to use gitmode and add upstream public key

 -- Birger Schacht <birger@debian.org>  Fri, 08 Jul 2022 13:11:17 +0200

mako-notifier (1.6-2) unstable; urgency=medium

  * Drop -Dapparmor from d/rules file (Closes: #998580)
  * Enable build and installation of fish shell completion files
  * Install systemd service file and apparmor rules file using debhelper
  * Update standards-version to 4.6.0.1 (no changes required)

 -- Birger Schacht <birger@debian.org>  Sat, 06 Nov 2021 11:29:37 +0100

mako-notifier (1.6-1) unstable; urgency=medium

  * New upstream release
  * Switch to git-bare repository layout
  * Update uploader email address
  * Switch to standards-version 4.5.1 (no changes required)
  * Bump debhelper-compat version to 13
  * Let the binary package declare that it provides a notification-daemon
    (Closes: #966335)
  * Add a lintian override for the dbus notification service file

 -- Birger Schacht <birger@debian.org>  Tue, 31 Aug 2021 21:28:31 +0200

mako-notifier (1.4.1-1) unstable; urgency=medium

  * New upstream release
  * d/control: bump standards version (no changes required)
  * d/upstream/metadata: Add Bug-Submit information

 -- Birger Schacht <birger@rantanplan.org>  Mon, 04 May 2020 11:51:30 +0200

mako-notifier (1.4-2) unstable; urgency=medium

  * Source only upload as-is.

 -- Birger Schacht <birger@rantanplan.org>  Fri, 17 Apr 2020 21:21:53 +0200

mako-notifier (1.4-1) unstable; urgency=medium

  * Initial release. (Closes: #912865)

 -- Birger Schacht <birger@rantanplan.org>  Fri, 03 Jan 2020 12:33:36 +0100
